/*
 * zzllrr Mather
 * zzllrr@gmail
 * Released under MIT License
 */

 

culture['Music']=Table([i18(ZLR('Name Type Summary'))],[
	[href(Hs+'www.autopiano.cn','Auto Piano 自由钢琴'),'JS + HTML',github('WarpPrism/AutoPiano')],

	[href(Hs+'bitmidi.com','Bit Midi'),'MIDI','Serving 113,241 MIDI files curated by volunteers around the world'],

],'TBrc');

	
