/*
 * zzllrr Mather
 * zzllrr@gmail
 * Released under MIT License
 */

culture['Museum']=Table([i18(ZLR('Name Type Features'))],[

	[href(Hs+'momath.org/explore/exhibits/','Museum of Mathematics - MoMath'),'',href(Hs+'momath.org/gallery/','Photo Gallery')+' '+href(Hs+'mathmidway.org/mm2go/exhibits.php','Math Midway 2 Go - mm2go (Exhibits)')],


	
	[href(Hs+'www.astc.org','Association of Science & Technology Centers - ASTC'),'Community',href(Hs+'www.astc.org/about-astc/about-science-centers/find-a-science-center/','local center')],

	[href(Hs+'www.cdstm.cn','China S & T Museum - 中国科技馆'),'',href(Hs+'www.cdstm.cn/xnxs/vr/','VR')],

	[href(Hs+'www.chnmuseum.cn/qwjs/?searchword=%E6%95%B0%E5%AD%A6','National Museum of China 中国国家博物馆'),'',''],
	[href(Hs+'www.louvre.fr/zh/selections/%E9%95%87%E9%A6%86%E4%B9%8B%E5%AE%9D','louvre Museum 卢浮宫博物馆'),'',''],
	[href(Hs+'www.britishmuseum.org/with_google.aspx','British Museum 大英博物馆'),'',href(Hs+'britishmuseum.withgoogle.com','with Google')],
	[href('http://www.metmuseum.org','MET 纽约大都会博物馆'),'',''],
	[href('http://hermitagemuseum.org','Hermitage 艾尔米塔什博物馆'),'',''],
	[href('http://mv.vatican.va','Vatican 梵蒂冈博物馆'),'',''],
	[href('http://www.musee-armee.fr/cn/home.html','Musee-Armee 法兰西军事博物馆'),'',''],
	[href('http://www.neues-museum.de','Neues Museum 柏林新博物馆'),'',''],
	[href('http://irannationalmuseum.ir','Iran National Museum 伊朗国家博物馆'),'',''],
	[href(Hs+'munchmuseet.no','Munch Museet挪威蒙克美术馆'),'',''],
	[href(Hs+'www.vmfa.museum','Verginia 美国弗吉尼亚美术馆'),'',''],
	[href('http://www.montargisvr.com','中国旅法勤工俭学蒙达尔纪纪念馆'),'',''],

	

	[href(Hs+'artsandculture.google.com','Google Culture Institute'),'','']

],'TBrc');
