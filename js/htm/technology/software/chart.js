/*
 * zzllrr Mather
 * zzllrr@gmail
 * Released under MIT License
 */

technology['Software/Charts']=Table([i18(ZLR('Tool Type Summary'))],[

	[inhref('technology.html?q=Software/Math','Math Software List'),'Math',''],
	[href(Hs+'observablehq.com',i18('Visualization Notebook')+' - Observable'),'','magic notebook for visualization '+href(Hs+'explorabl.es','Explorable Explanations')+github('d3/d3/wiki/Gallery')],
	[href(H+'www.fooplot.com','Foo Plot'),'Web','在线画函数图像'],

	[href(Hs+'sketchfab.com',i18('3D Model')+' - Sketchfab'),'3D','publish, share and embed interactive 3D files'],
	[href(Hs+'hightopo.com',i18('3D Model')+' - HighTopo图扑'),'工程','create cutting-edge 2D and 3D visualization '+href(H+'hightopo.com/demos/index.html','Examples')],

	[href(Hs+'draw.io','draw.io'),'JS + SVG','Create flow charts, process diagrams, org charts, UML diagrams, ER models, network diagrams'],
	[href(Hs+'demo.bpmn.io/s/start','bpmn.io'),'JS + SVG',hrefA('demo.bpmn.io/@@/new',ZLR('BPMN CMMN DMN')).join(' ')],

	[href(Hs+'naotu.baidu.com','百度脑图'),'','Mindmap by baidu'],

	[href(Hs+'witeboard.com','WiteBoard'),'','Whiteboard, the same URL could be shared with synchronization'],

	[github('paveldogreat','WebGL-Fluid-Simulation','WebGL Fluid Simulation'),'JS + WebGL',github('PavelDoGreat/WebGL-Fluid-Simulation')],
	[href(H+'weavesilk.com','WeaveSilk'),'','Interactive Generative Art, doodle with music'],


],'TBrc')+
detail(gM('Reference'),Table([i18(ZLR('Name Type Summary'))],[

	[href(Hs+'bestofjs.org','Best of JS'),'Nav','Best of JavaScript is a project by Michael Rambeau, made in Osaka, Japan'],

],'TBrc'),1);

